#include <SDL.h>
#include <SDL_mixer.h>
#include <iostream>
#include "AudioPlayerComponent.h"
#include "../Systems/AudioPlayerSystem.h"
#include "../Core/Mathematics.h"

void AudioPlayerComponent::Play(AudioChunk* chunk, float distance){
	std::cout << "Audio has been received by the audio system to the player.\n";
	chunks.push_back(AudioPlayerChunk());
	int i = chunks.size() - 1;
	chunks[i].chunk = chunk;
	chunks[i].distance = distance;
	chunks[i].state = PlayState::Stopped;
	chunks[i].channel = AudioPlayerSystem::GetInstance()->GetFreeChannel();

	Vector3 direction = chunks[i].chunk->position - position;

	if(chunks[i].distance != -1){
		chunks[i].volumeMod = 1 - (chunks[i].distance / chunks[i].chunk->distance);

		chunks[i].leftPan = 1 - direction.x / chunks[i].distance;

		if(chunks[i].leftPan > 1){
			//The panning is to the right
			chunks[i].rightPan = 1 + direction.x / chunks[i].distance;

			chunks[i].leftPan = (1 - chunks[i].rightPan) * 255;
			chunks[i].rightPan *= 255;
		}
		else {
			chunks[i].rightPan = (1 - chunks[i].leftPan) * 255;

			chunks[i].leftPan *= 255;
		}
	}
	else {
		chunks[i].leftPan = 255;
		chunks[i].rightPan = 255;
	}
}

void AudioPlayerComponent::Update(){

	for(int i = 0; i < chunks.size(); i++){
		//If the audio is stopped, delete it from the vector
		if(chunks[i].chunk->playState == PlayState::Stopped){
			Mix_HaltChannel(chunks[i].channel);
			AudioPlayerSystem::GetInstance()->UpdateChannelIndex(chunks[i].channel, false);
			chunks.erase(chunks.begin() + i);
			std::cout << "Erasing audio from player.\n";
			return;
		}
		//If the audio is paused, pause it
		if(chunks[i].chunk->playState == PlayState::Paused && chunks[i].state != PlayState::Paused){
			chunks[i].state = PlayState::Paused;
			Mix_Pause(chunks[i].channel);
		}
		//If the audio is resumed, resume it
		if(chunks[i].chunk->playState == PlayState::Playing && chunks[i].state == PlayState::Paused){
			chunks[i].state = PlayState::Playing;
			Mix_Resume(chunks[i].channel);
		}
	}
	
	for(int i = 0; i < chunks.size(); i++){
		//If each audio source is playing, play them at this components' respective channels.
		if(chunks[i].chunk->playState == PlayState::Playing && chunks[i].state == PlayState::Stopped){
			//If the distance is -1, this is a mono sound that should just be played at default volume
			if(chunks[i].distance == -1){
				Mix_Volume(chunks[i].channel, MIX_MAX_VOLUME * chunks[i].chunk->volume);
				Mix_SetPanning(chunks[i].channel, chunks[i].leftPan, chunks[i].rightPan);
				if(Mix_PlayChannel(chunks[i].channel, chunks[i].chunk->mixerChunk, chunks[i].chunk->loops) == -1){
					std::cout << "ERROR at channel: " << chunks[i].channel << " with error: " << Mix_GetError() << std::endl;
				}
				else {
					std::cout << "Playing mono sound at volume " << chunks[i].chunk->volume << " at channel " << chunks[i].channel << " with panning of " << chunks[i].leftPan << "," << chunks[i].rightPan << std::endl;
				}
			}
			else {
				//Distance/volume calc here. volumeMod will be a value from 0-1 where 0 is out of range and 1 is on top of the player
				Mix_SetPanning(chunks[i].channel, chunks[i].leftPan, chunks[i].rightPan);
				Mix_Volume(chunks[i].channel, MIX_MAX_VOLUME * chunks[i].chunk->volume * chunks[i].volumeMod);
				if(Mix_PlayChannel(chunks[i].channel, chunks[i].chunk->mixerChunk, chunks[i].chunk->loops) == -1){
					std::cout << "ERROR at channel: " << chunks[i].channel << " with error: " << Mix_GetError() << std::endl;
				}
				else {
					std::cout << "Playing sound at volume " << chunks[i].chunk->volume * chunks[i].volumeMod << " at channel " << chunks[i].channel << " with panning of " << chunks[i].leftPan << "," << chunks[i].rightPan << std::endl;
				}
			}
			chunks[i].state = PlayState::Playing;
		}
	}
}

AudioPlayerComponent::AudioPlayerComponent(){
	position = VECTOR3_ZERO;
	AudioPlayerSystem::GetInstance()->AddPlayer(this);
	std::cout << "Created audio player\n";
}

AudioPlayerComponent::~AudioPlayerComponent(){
	for(int i = 0; i < chunks.size(); i++){
		AudioPlayerSystem::GetInstance()->UpdateChannelIndex(chunks[i].channel, false);
	}
}