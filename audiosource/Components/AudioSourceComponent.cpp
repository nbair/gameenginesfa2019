#include "AudioSourceComponent.h"
#include "../Systems/AudioPlayerSystem.h"
#include <iostream>

AudioSourceComponent::AudioSourceComponent(Vector3 position, float distance, float volume, std::string fileName, int loops){
	chunk.volume = volume;
	chunk.position = position;
	chunk.distance = distance;
	chunk.mixerChunk = Mix_LoadWAV(fileName.c_str());
	chunk.loops = loops;
	chunk.playState = PlayState::Stopped;

	std::cout << "Created Audio Source\n";
}

AudioSourceComponent::~AudioSourceComponent(){
	Mix_FreeChunk(chunk.mixerChunk);
	chunk.mixerChunk = NULL;
}

void AudioSourceComponent::Play(){
	//If we are paused then we don't want to send it again. The player will check it at this point
	if(chunk.playState == PlayState::Stopped){
		std::cout << "Sending Audio Source to System\n";
		AudioPlayerSystem::GetInstance()->SendSound(&chunk);
	}
	else {
		std::cout << "Resuming audio\n";
	}
	chunk.playState = PlayState::Playing;
}

void AudioSourceComponent::Pause(){
	std::cout << "Pausing audio.\n";
	chunk.playState = PlayState::Paused;
}

void AudioSourceComponent::Stop(){
	std::cout << "Stopping audio.\n";
	chunk.playState = PlayState::Stopped;
}