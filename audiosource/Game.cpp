#include "Game.h"
#include <assert.h>
#include <iostream>
#include <time.h>

#include "Core/Mathematics.h"
#include "Systems/AudioPlayerSystem.h"
#include "Components/AudioSourceComponent.h"
#include "Components/AudioPlayerComponent.h"

Game::Game(int width, int height){
	SDL_Init(SDL_INIT_EVERYTHING);

	AudioPlayerSystem::InitInstance();
	audioSystem = AudioPlayerSystem::GetInstance();
	audioSystem->init();

	audioSource = std::make_shared<AudioSourceComponent>(Vector3(8, 0, 0), 10, 1, "temp.wav");
	audioSource2 = std::make_shared<AudioSourceComponent>(Vector3(-9, 0, 0), 10, 1, "temp2.wav");
	audioPlayer = std::make_shared<AudioPlayerComponent>();

	window = SDL_CreateWindow("AudioSystem", 50, 50, width, height, SDL_WINDOW_RESIZABLE);

	audioSource->Play();
	audioSource2->Play();

	stopped = false;

	elapsedTime = 0;
}

Game::~Game(){
	
	audioSystem->cleanup();
	SDL_DestroyWindow(window);

	SDL_Quit();

	stopped = true;
}

void Game::GameLoop(){
	while(!stopped){
		elapsedTime = SDL_GetTicks();
		SDL_Event event;
		if(SDL_PollEvent(&event)){
			switch(event.type){
			case SDL_QUIT:
				delete this;
				return;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym){
				case SDLK_ESCAPE:
					delete this;
					return;
				case SDLK_1:
					audioSource->Stop();
					break;
				case SDLK_2:
					audioSource2->Stop();
					break;
				case SDLK_3:
					audioSource->Play();
					break;
				case SDLK_4:
					audioSource2->Play();
					break;
				case SDLK_5:
					audioSource->Pause();
					break;
				case SDLK_6:
					audioSource2->Pause();
					break;
				}
				
			}
		}
		//Update systems
		audioSystem->Update();
		SDL_Delay(TimeLeft());
	}
}

Uint32 Game::TimeLeft(){
	elapsedTime = SDL_GetTicks() - elapsedTime;
	if(elapsedTime > TIMEPERFRAME){
		return 0;
	}
	else{
		return elapsedTime;
	}
}
