#include <memory>
#include <SDL.h>

class AudioPlayerSystem;
class AudioSourceComponent;
class AudioPlayerComponent;

class Game {

	public:
		Game(int width, int height);
		~Game();
		void GameLoop();
	private:
		Uint32 TimeLeft();

		Uint32 elapsedTime;
		bool stopped = false;

		std::shared_ptr<AudioPlayerSystem> audioSystem;
		std::shared_ptr<AudioSourceComponent> audioSource, audioSource2;
		std::shared_ptr<AudioPlayerComponent> audioPlayer;

		const double TIMEPERFRAME = 16.67;

		SDL_Window* window;
};