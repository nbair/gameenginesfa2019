#include "AudioPlayerSystem.h"
#include "../Core/Mathematics.h"
#include "../Components/AudioPlayerComponent.h"
#include "../Components/AudioSourceComponent.h"
#include <iostream>

std::shared_ptr<AudioPlayerSystem> AudioPlayerSystem::instance = NULL;

void AudioPlayerSystem::init(int frequency){
	Mix_Init(0);
	Mix_OpenAudio(frequency, MIX_DEFAULT_FORMAT, MAX_CHANNELS, 1024);
	for(int i = 0; i < MAX_CHANNELS; i++){
		channels.push_back(false);
	}
	std::cout << "Audio Player inited. Number of channels " << MAX_CHANNELS << std::endl;
}

void AudioPlayerSystem::cleanup(){
	Mix_CloseAudio();
	Mix_Quit();
	std::cout << "Audio Player closed.\n";
}

void AudioPlayerSystem::SendSound(AudioChunk* chunk){
	if(chunk->distance == -1){
		//Send it to the first audio player initialized and play it. (This is mono sound)
		for(int i = 0; i < channelPlayers.size(); i++){
			channelPlayers[i]->Play(chunk, -1);
			break;
		}
	}
	else {
		//Check each audio player's distance. If we found one within the distance, send the play function to that component
		for(int i = 0; i < channelPlayers.size(); i++){
			float dist = distance(channelPlayers[i]->GetPosition(), chunk->position);
			if(dist <= chunk->distance){
				channelPlayers[i]->Play(chunk, dist);
			}
		}
	}
}

void AudioPlayerSystem::UpdateChannelIndex(int index, bool enabled){
	channels[index] = enabled;
}

void AudioPlayerSystem::InitInstance(){
	instance = std::make_shared<AudioPlayerSystem>();
}

std::shared_ptr<AudioPlayerSystem> AudioPlayerSystem::GetInstance(){
	return instance;
}

int AudioPlayerSystem::GetFreeChannel(){
	for(int i = 0; i < MAX_CHANNELS; i++){
		if(channels[i] == false){
			channels[i] = true;
			return i;
		}
	}
	return -1;
}

void AudioPlayerSystem::AddPlayer(AudioPlayerComponent* player){
	channelPlayers.push_back(player);
}

void AudioPlayerSystem::Update(){
	//Iterate through the list of initialized players. Update them
	for(int i = 0; i < channelPlayers.size(); i++){
		channelPlayers[i]->Update();
	}
}