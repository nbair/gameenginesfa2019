#define SDL_MAIN_HANDLED

#include <iostream>
#include "Game.h"

// engine main called by others
int CamelMain()
{
	// do something stupid (i.e. put the game in a memory pool)
	Game* game = new Game(400, 600);
	game->GameLoop();
	return 42;
}

// compiling on MacOS or Linux/Unix
int main()	
{
	std::cout << "Console!" << std::endl;

	return CamelMain();
}

#ifdef _WIN32

#include <Windows.h>

// compiling on Windows
int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR szCmdLine, int sw)
{
	//MessageBox(NULL, szCmdLine, "Windows!", 0);

	return CamelMain();
}

#endif // _WIN32