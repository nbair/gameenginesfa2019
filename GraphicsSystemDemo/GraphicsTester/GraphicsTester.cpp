

#include <iostream>
#include "../RenderComponent.h"
#include "../RenderSystem.h"
int main()
{

	auto rs = new RenderSystem();
	rs->init(800, 600);
	auto rc = new RenderComponent();
	rc->init();
	auto rctwo = new RenderComponent();
	rctwo->init();
	float arr[] = {
	-1.0f, -1.0f, 0.0f,
	 -0.5f, -0.5f, 0.0f,
	 -1.0f,  0.5f, 0.0f
	};
	// Set trianglular "mesh" to given vertices.
	rctwo->setMesh(arr, 9);
	rs->addRenderComp(rctwo);

	//set triangle to default vertices.
	rc->setMesh();
	rs->addRenderComp(rc);

	while (rs->draw());
	rc->cleanup();
	rctwo->cleanup();
	rs->cleanup();
}
