rmdir /s /Q ".vs"
rmdir /s /Q "CamelEngine.dir"
rmdir /s /Q "Debug"
rmdir /s /Q "CMakeFiles"
rmdir /s /Q "x64"

del /Q *.filters
del /Q *.vcxproj
del /Q *.sln
del /Q *.user
del /Q cmake_install.cmake
del /Q CMakeCache.txt
