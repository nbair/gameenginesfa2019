#pragma once
#include "Component.h"
#include "../Components/AudioSourceComponent.h"
#include <vector>

struct AudioPlayerChunk {
	AudioChunk* chunk;
	PlayState state;
	int channel;
	int distance;
};

//Require the transform component in order to make this work
class AudioPlayerComponent : Component {
	public:
		//Initialize and get a free channel from the system
		AudioPlayerComponent();

		//Destroy and remove the channel from the system
		~AudioPlayerComponent();

		//Play a certain sound (usually called from the AudioPlayerSystem, although could be called directly if the user wants.
		void Play(AudioChunk* chunk, int distance);

		friend class AudioPlayerSystem;

		Vector3 GetPosition() { return position; };

	private:
		//Update the player.
		void Update(int number);
		std::vector<AudioPlayerChunk> chunks;

		int updateIndex;

		Vector3 position;
};