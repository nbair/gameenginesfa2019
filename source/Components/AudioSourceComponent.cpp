#include "AudioSourceComponent.h"
#include "../Systems/AudioPlayerSystem.h"

AudioSourceComponent::AudioSourceComponent(Vector3 position, float distance, float volume, std::string fileName, int loops){
	chunk.volume = volume;
	chunk.position = position;
	chunk.distance = distance;
	chunk.mixerChunk = Mix_LoadWAV(fileName.c_str());
	chunk.loops = loops;
}

AudioSourceComponent::~AudioSourceComponent(){
	Mix_FreeChunk(chunk.mixerChunk);
	chunk.mixerChunk = NULL;
}

void AudioSourceComponent::Play(){
	//If we are paused then we don't want to send it again. The player will check it at this point
	if(chunk.playState == PlayState::Stopped){
		AudioPlayerSystem::GetInstance()->SendSound(&chunk);
	}
	chunk.playState = PlayState::Playing;
}

void AudioSourceComponent::Pause(){
	chunk.playState = PlayState::Paused;
}

void AudioSourceComponent::Stop(){
	chunk.playState = PlayState::Stopped;
}