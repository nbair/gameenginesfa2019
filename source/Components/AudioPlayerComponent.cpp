#include <SDL.h>
#include <SDL_mixer.h>
#include <iostream>
#include "AudioPlayerComponent.h"
#include "../Systems/AudioPlayerSystem.h"
#include "../Core/Mathematics.h"

void AudioPlayerComponent::Play(AudioChunk* chunk, int distance){
	std::cout << "Chunk has been received by the audio player.\n";
	chunks.push_back(AudioPlayerChunk());
	int i = chunks.size() - 1;
	chunks[i].chunk = chunk;
	chunks[i].distance = distance;
	chunks[i].state = PlayState::Stopped;
	AudioPlayerSystem::GetInstance()->GetFreeChannel(this);
	updateIndex = MAX_CHANNELS;
}

void AudioPlayerComponent::Update(int index){
	if(index > updateIndex){
		return;
	}
	
	updateIndex = index;

	for(int i = 0; i < chunks.size(); i++){
		//If the audio is stopped, delete it from the vector
		if(chunks[i].chunk->playState == PlayState::Stopped){
			AudioPlayerSystem::GetInstance()->UpdateChannelIndex(chunks[i].channel, nullptr);
			chunks.erase(chunks.begin() + i);
		}
		//If the audio is paused, pause it
		if(chunks[i].chunk->playState == PlayState::Paused && chunks[i].state != PlayState::Paused){
			chunks[i].state = PlayState::Paused;
			Mix_Pause(i);
		}
		//If the audio is resumed, resume it
		if(chunks[i].chunk->playState == PlayState::Playing && chunks[i].state == PlayState::Paused){
			chunks[i].state = PlayState::Playing;
			Mix_Pause(i);
		}
	}
	
	for(int i = 0; i < chunks.size(); i++){
		//If each audio source is playing, play them at this components' respective channels.
		if(chunks[i].chunk->playState == PlayState::Playing && chunks[i].state == PlayState::Stopped){
			//If the distance is -1, this is a mono sound that should just be played at default volume
			if(chunks[i].distance == -1){
				Mix_Volume(chunks[i].channel, MIX_MAX_VOLUME * chunks[i].chunk->volume);
				if(Mix_PlayChannel(chunks[i].channel, chunks[i].chunk->mixerChunk, chunks[i].chunk->loops) == -1){
					std::cout << "ERROR at channel: " << chunks[i].channel << " with error: " << Mix_GetError() << std::endl;
				}
				else {
					std::cout << "Playing mono sound at volume " << chunks[i].chunk->volume << " at channel " << chunks[i].channel << std::endl;
				}
			}
			else {
				//Distance/volume calc here. volumeMod will be a value from 0-1 where 0 is out of range and 1 is on top of the player
				float volumeMod = 1 - (chunks[i].distance / chunks[i].chunk->distance);
				Mix_Volume(chunks[i].channel, MIX_MAX_VOLUME * chunks[i].chunk->volume * volumeMod);
				if(Mix_PlayChannel(chunks[i].channel, chunks[i].chunk->mixerChunk, chunks[i].chunk->loops) == -1){
					std::cout << "ERROR at channel: " << chunks[i].channel << " with error: " << Mix_GetError() << std::endl;
				}
				else {
					std::cout << "Playing sound at volume " << chunks[i].chunk->volume * volumeMod << " at channel " << chunks[i].channel << std::endl;
				}
			}
		}
	}
}

AudioPlayerComponent::AudioPlayerComponent(){
	position = VECTOR3_ZERO;
}

AudioPlayerComponent::~AudioPlayerComponent(){
	for(int i = 0; i < chunks.size(); i++){
		AudioPlayerSystem::GetInstance()->UpdateChannelIndex(chunks[i].channel, nullptr);
	}
}