#pragma once

#include "SDL_mixer.h"
#include "Component.h"
#include "../Core/Mathematics.h"
#include <string>

//An audio source component will broadcast an audio at a specific position. This will be grabbed by an audio player to be played. If you want it to be mono set the distance to -1
//Clamp volume 0-1

enum PlayState {Stopped, Playing, Paused};

struct AudioChunk {
	Mix_Chunk* mixerChunk;
	Vector3 position;
	float distance;
	float volume;
	PlayState playState;
	int loops;
};

class AudioSourceComponent : Component {

	public:
		//Initialize the component with a position of the sound, a distance the sound can travel, a default volume, a filename to load the sound, and whether or not it loops. 
		//For mono sounds pass -1 as distance. Position is usually just the position of the component. Mono sounds can pass a position or just VECTOR3_ZERO. -1 is infinite loops.
		//Clamp volume 0-1
		AudioSourceComponent(Vector3 position, float distance, float volume, std::string fileName, int loops = 0);
		~AudioSourceComponent();

		//Setters
		void SetDistance(float newDist) { chunk.distance = newDist; };
		void SetVolume(float newVolume) { chunk.volume = newVolume; };
		Vector3 SetPosition(Vector3 newPos) { chunk.position = newPos; };

		//Getters
		float GetDistance() { return chunk.distance; };
		float GetVolume() { return chunk.volume; };
		Vector3 GetPosition() { return chunk.position; };

		//Broadcast the audio to the players
		void Play();

		//Stop the audio
		void Stop();

		//Pause the audio
		void Pause();

	private:
		AudioChunk chunk;
};