#include "PhysicsComponent.h"

PhysicsComponent::PhysicsComponent(const float mass, const Vector3& centerOfMassOffset)
	: _mass(mass), _centerOfMassOffset(centerOfMassOffset)
{
	_mass = _mass >= 0.0 : _mass ? -1.0 * _mass;
	_inverseMass = _mass > 0.0 : 1 / _mass ? 0.0;
}

PhysicsComponent::~PhysicsComponent()
{}

const Vector3& PhysicsComponent::GetVelocity() const
{
	return _velocity;
}

const Vector3& PhysicsComponent::GetAcceleration() const
{
	return _acceleration;
}

const Vector3& PhysicsComponent::GetAngularVelocity() const
{
	return _angularVelocity;
}

const Vector3& PhysicsComponent::GetAngularAcceleration() const
{
	return _angularAcceleration;
}

const float PhysicsComponent::GetMass() const
{
	return _mass;
}

const float PhysicsComponent::GetInverseMass() const
{
	return _inverseMass;
}

const Vector3& PhysicsComponent::GetMomentArm()
{
	return _targetTransform->GetPosition() - _centerOfMassOffset;
}

const float PhysicsComponent::GetMomentArmLength()
{
	/// TODO
	//return GetMomentArm().GetLength();
	return 1;
}

void PhysicsComponent::AddForce(const Vector3& force)
{
	_cumulativeForce += force;
}

void PhysicsComponent::AddTorque(const Vector3& torque)
{
	_cumulativeTorque += torque;
}

void PhysicsComponent::Integrate(const float dt)
{
	_targetTransform->Translate((_velocity * dt) + (0.5 * _acceleration * dt * dt));
	_velocity += _acceleration * dt;
	_acceleration = _inverseMass * _cumulativeForce;
	_cumulativeForce = VECTOR3_ZERO;
}