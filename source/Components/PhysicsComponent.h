#pragma once

#include "Component.h"
#include "TransformComponent.h"
#include "../Core/Mathematics.h"

class PhysicsComponent : public Component
{
	public:

		~PhysicsComponent();

		const Vector3& GetVelocity() const;

		const Vector3& GetAcceleration() const;

		const Vector3& GetAngularVelocity() const;

		const Vector3& GetAngularAcceleration() const;

		const float GetMass() const;

		const float GetInverseMass() const;

		const Vector3& GetMomentArm();

		const float GetMomentArmLength();

		void AddForce(const Vector3& force);

		void AddTorque(const Vector3& torque);

		void Integrate(const float dt);

	private:

		PhysicsComponent(float mass = 1.0, const Vector3& centerOfMassOffset = VECTOR3_ZERO);

		TransformComponent* _targetTransform;

		float _mass;

		float _inverseMass;

		Vector3 _centerOfMassOffset;

		Vector3 _velocity;

		Vector3 _acceleration;

		Vector3 _angularVelocity;

		Vector3 _angularAcceleration;

		Vector3 _cumulativeForce;
		
		Vector3 _cumulativeTorque;
};