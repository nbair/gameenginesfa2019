#pragma once

#include "Component.h"
#include "../Core/Mathematics.h"

class TransformComponent : public Component
{
	public:

		~TransformComponent();

		const Vector3& GetPosition() const;

		const Quaternion& GetOrientation() const;

		const Vector3& EulerAngles() const;

		void Translate(const Vector3& translation);

		void Rotate(const Quaternion rotation);

	private:

		TransformComponent(const Vector3& position = VECTOR3_ZERO, const Quaternion& orientation = QUATERNION_IDENTITY);

		Vector3 _position;

		Quaternion _orientation;
};