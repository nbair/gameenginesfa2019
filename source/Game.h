#include <memory>
#include <SDL.h>
#include "Systems/RenderSystem.h"

class AudioPlayerSystem;

class Game {

	public:
		Game(int width, int height);
		~Game();
		void GameLoop();
	private:
		Uint32 TimeLeft();

		Uint32 elapsedTime;
		bool stopped = false;

		std::unique_ptr<RenderSystem> renderer;
		std::shared_ptr<AudioPlayerSystem> audioPlayer;

		const double TIMEPERFRAME = 16.67;
};