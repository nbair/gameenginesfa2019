#pragma once

#include "../Components/AudioPlayerComponent.h"
#include "../Components/AudioSourceComponent.h"
#include <memory>
#include <SDL_mixer.h>
#include <map>

const int MAX_CHANNELS = 6;

class AudioPlayerComponent;

class AudioPlayerSystem {
public:
	//Initialize (through the shared pointer - GetInstance)
	void init(int frequency = MIX_DEFAULT_FREQUENCY);
	//Cleanup (through the shared pointer - GetInstance)
	void cleanup();

	//Update each audio player
	void Update();

	//Send the sound out to the audio players
	void SendSound(AudioChunk* chunk);

	//Either fill a channel or delete one. Pass null if we are deleting it
	void UpdateChannelIndex(int index, AudioPlayerComponent* player);

	//Get the first free channel and pass it to the player. If no free channels, pass -1
	int GetFreeChannel(AudioPlayerComponent* player);

	//Statically initialize the audio player
	static void InitInstance();
	//Get the shared ptr of the initialized player
	static std::shared_ptr<AudioPlayerSystem> GetInstance();

private:
	std::vector<AudioPlayerComponent*> channelPlayers;
	static std::shared_ptr<AudioPlayerSystem> instance;
};