#pragma once
#include <map>
#include <string>
#include <vector>

class RenderComponent;
class GLFWwindow;

class RenderSystem {
public:
	RenderSystem();
	~RenderSystem();
	bool init(int windowWidth, int windowHeight);
	bool cleanup();
	bool draw();
	bool LoadImage(std::string path, std::string texName);
	void DisplayImage(std::string imgName, int x, int y);

	void addRenderComp(RenderComponent* rc);
	void doMain();
private:
	GLFWwindow* window;

	unsigned int shaderProgram;

	bool initShaderProgram();
	std::vector<RenderComponent*> renderComponents;
};