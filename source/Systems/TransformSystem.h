#pragma once

#include "../Components/TransformComponent.h"
#include "../Core/Mathematics.h"

class TransformSystem
{
	public:

		static bool startup() { return true; };

		static void update(float dt) {};

		static bool shutdown() { return true; };

		static const Vector3& translate(const int operand, const Vector3& translation);

		static const Quaternion& rotate(const int operand, const Quaternion& rotation);
};