#pragma once

#include "../Components/PhysicsComponent.h"
#include "Mathematics.h"

class PhysicsSystem
{
	public:

		static void integrate(const int operand);
};