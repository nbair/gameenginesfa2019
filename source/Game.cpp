#include "Game.h"
#include <assert.h>
#include <iostream>
#include <time.h>

#include "Systems/TransformSystem.h"
#include "Components/TransformComponent.h"
#include "Systems/AudioPlayerSystem.h"

Game::Game(int width, int height){
	SDL_Init(SDL_INIT_EVERYTHING);

	renderer = std::make_unique<RenderSystem>();
	AudioPlayerSystem::InitInstance();
	audioPlayer = AudioPlayerSystem::GetInstance();
	audioPlayer->init();

	renderer->init(width, height);
	stopped = false;
	// sample use case
	// TransformComponent* transform = new TransformComponent();
	// TransformSystem::translate(transform, new Vector3());

	elapsedTime = 0;
}

Game::~Game(){
	
	renderer->cleanup();
	audioPlayer->cleanup();

	SDL_Quit();

	stopped = true;
}

void Game::GameLoop(){
	while(!stopped){
		elapsedTime = SDL_GetTicks();
		SDL_Event event;
			if(SDL_PollEvent(&event)){
				switch(event.type){
				case SDL_QUIT:
					delete this;
					return;
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym){
					case SDLK_ESCAPE:
						delete this;
						return;
					}
				}
			}
			//Update systems
			renderer->draw();
			SDL_Delay(TimeLeft());
	}
}

Uint32 Game::TimeLeft(){
	elapsedTime = SDL_GetTicks() - elapsedTime;
	if(elapsedTime > TIMEPERFRAME){
		return 0;
	}
	else{
		return elapsedTime;
	}
}
