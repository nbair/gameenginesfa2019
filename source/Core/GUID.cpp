#include "GUID.h"
#include<sstream>
#include<random>

unsigned int GUID::GenerateGUID(int length)
{
	return 0;
}

int GUID::RandomChar()
{
	//https://en.cppreference.com/w/cpp/numeric/random/random_device
	std::random_device randomDevice; //Creates the random device

	//http://www.cplusplus.com/reference/random/mt19937/
	//mt19937 is basicly a random generator for generating 32bit numbers with a state size of 19937 bits
	std::mt19937 gen(randomDevice()); //Generates our random number based

	//http://www.cplusplus.com/reference/random/uniform_int_distribution/
	//The function for generating the actuall random number
	std::uniform_int_distribution<> dis(0, 255);

	//Returns the random number using the paramaters passed in?
	return dis(gen);
}
