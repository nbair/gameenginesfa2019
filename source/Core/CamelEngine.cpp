#include "CamelEngine.h"

CamelEngine::CamelEngine()
{
	std::cout << "CamelEngine has been created!" << std::endl;

	_transformSystem = new TransformSystem();
}

CamelEngine::~CamelEngine()
{
	std::cout << "CamelEngine has been destroyed!" << std::endl;

	delete _transformSystem;
}

bool CamelEngine::startup()
{
	std::cout << "CamelEngine is starting up..." << std::endl;

	bool systemsValid = TransformSystem::startup();

	return systemsValid;
}

void CamelEngine::tick()
{
	float dt = 0.1;

	std::cout << "CamelEngine is updating all systems..." << std::endl;

	TransformSystem::update(dt);
}

bool CamelEngine::shutdown()
{
	std::cout << "CamelEngine is shutting down..." << std::endl;

	bool systemsValid = TransformSystem::shutdown();

	return systemsValid;
}