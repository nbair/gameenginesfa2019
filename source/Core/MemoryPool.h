#pragma once
#include <iostream>
#include <map>
#include "../Core/MemoryManager.h"

/*
--Memory pool--
-This will be an individual memory pool
-Will be templated so the user can use it with any memory class they need
*/

/*
--KEY for priority
-LOW - not needed for functionality
-MEDIUM - needed for functionality but no implemtation doesn't effect architecture
-HIGH - needed for functionality & effects architecture in a major way
*/

/*
--BIG to do's
-Set up actuall memory manager (MEDIUM)
-Pointers for entry (preferably smart pointers) (HIGH)
-Get size of class passed in for memory pool (LOW)
-Fix ID issue (HIGH) (https://lowrey.me/guid-generation-in-c-11/) - Potential fix
*/

/*
--Potential issues with current architecture
-ID issues where an entry can potentiall have the id of an old entry, this may cause shenaigans with entities trying to access an entry that doesn't exist... But techicly does
*/

template<class T>
class MemoryPool
{
public:
	MemoryPool();
	MemoryPool(T dataType, int maxEntriesNew);
	~MemoryPool();

	//Pool management
	void init(T dataType, int maxEntriesNew); //Initiates the pool
	void cleanup(); //Destroys the pool 
	void resize();

	//Entry management & functions
	int createEntry(int id); //Creates the entry
	T getEntry(int id); //Returns an entry based on the inputed id
	void freeEntry(int id);

private:
	struct entry
	{
		int entrySize = 0;
		int entryPosition;
		T entryData;
		int id;

		entry(int newId, int size, int position) { id = newId; entrySize = size; entryPosition = position; };
		entry(int id, int size, int position, T data) { id = newId; entrySize = size; entryPosition = position; entryData = data};
	};

	int entryCount = 0;
	int maxEntries = 0;
	int entrySize = 1; //The size of each entry
	std::map<int, entry> entryMap;

	int findFreeSpot();
	void defragPool(); 

};


