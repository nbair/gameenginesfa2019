#include "..\Core\MemoryPool.h"

template<class T>
MemoryPool<T>::MemoryPool()
{
	//Creates without any info
}

template<class T>
MemoryPool<T>::MemoryPool(T dataType, int maxEntries)
{
	init(dataType, maxEntries);
}

template<class T>
MemoryPool<T>::~MemoryPool()
{
}

//PUBLIC
template<class T>
void MemoryPool<T>::init(T dataType, int maxEntriesNew)
{
	maxEntrys = maxEntriesNew;
	entryCount = 0;

	//TODO - get the memory size of the passed in datatype for the size of each pool
	entrySize = 1;

	//There's probably a better way to do this but it's something for now
	//Creates the map for the memory pool
	for (int i = 0; i < maxEntries; i++)
	{
		// Sets up a new entry with an id of -1, this will be set to the key when the entry is assigned
		entry newEntry(-1, entrySize, i); //Initiates the entry with the data size & position with null as an idenitfier 
		entryMap.insert(newEntry, i);
	}
}


//PRIVATE
template<class T>
inline int MemoryPool<T>::findFreeSpot()
{
	/*
		Run through the map and find the first free entry (will have id -1) then return the key
	*/

	return 0;
}

template<class T>
void MemoryPool<T>::defragPool()
{
	/*
		Will compress all of the
	*/
}

template<class T>
int MemoryPool<T>::createEntry(int id)
{
	/*
		1. Check for exit conditions
			- No free entries
			- Duplicant ID

		2. Allocate memory
		3. Add pointer to map
		4. Return ID

	*/

	return id;
}

template<class T>
T MemoryPool<T>::getEntry(int id)
{
	/*
		1. Check for exit conditions
			- Id doesn't exist
			- Pool empty
		2. Find the pointer in the map and return it
		4. profit
	*/
}

template<class T>
void MemoryPool<T>::freeEntry(int id)
{
	/*
		1. Check for exit condtions
			- ID doesn't exist
			- Pool empty
		2. Dealocate memory
		3. Remove from map
	*/
}