#pragma once

/*
--Memory Manager--
 - This will handle all the nitty gritty memory allocation
 - To be used along side the memory pool
*/


class Memorymanager
{
public:
	//Overloads the new and delete operators to use the custom memory manager
	void* operator new(size_t size);
	void operator delete(void* memory);

private:

	static void *allocateMemory();
	static void *deallocateMemory(void* memory);

};

