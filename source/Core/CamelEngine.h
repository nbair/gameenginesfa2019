#pragma once


#include "../Systems/Systems.h"

#include <iostream>

class CamelEngine
{
	public:

		CamelEngine();

		~CamelEngine();

	private:

		bool startup();

		void tick();

		bool shutdown();

		TransformSystem* _transformSystem;

		// all other systems here

		//MemoryPool<RenderComponent>* _renderComponents;
		// etc ...
};