#pragma once

#include <typeinfo>
#include <typeindex>
#include <unordered_map>
#include <list>

#include "../Components/Component.h"
#include "CamelEngine.h"

typedef int EntityID;

class GUID;

class Entity
{
	Entity();

	Entity(const std::string& name);

	Entity(const Entity& copy);

	Entity(const Entity& copy, const std::string& name);

	~Entity();

	template<class T> void AddComponent();

	template<class T> void RemoveComponent();

	template<class T> const T& GetComponent();

	private:

		GUID _id;

		std::string _name;

		std::unordered_map<std::type_index, GUID> _components;
};