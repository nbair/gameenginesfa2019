#pragma once

#include "Entity.h"
#include "CamelEngine.h"

class Scene
{
	public:

		Scene();

		~Scene();

		const EntityID& addEntity();

		const EntityID& addEntity(const EntityID& parentID);

		const Entity& findEntity(const EntityID& id);

		const EntityID& removeEntity(const EntityID& id);

	private:

		Entity* _root;
};