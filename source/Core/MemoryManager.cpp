#include "MemoryManager.h"

void* Memorymanager::operator new(size_t size)
{
	return allocateMemory();
}

void Memorymanager::operator delete(void* memory)
{
	deallocateMemory(memory);
}
