#include "Entity.h"

Entity::Entity()
{

}

Entity::Entity(const std::string& name) : _name(name)
{
	_id = GUID::CreateGUID;
}

Entity::Entity(const Entity& copy) : _name(copy._name.append("(Copy)"))
{
	_id = GUID::CreateGUID;

	for each (auto component in copy._components)
	{
		_components[typeid(component)] = CamelEngine::CreateComponent<typeid(component)>(_id);
	}
}

Entity::Entity(const Entity& copy, const std::string& name) : _name(name)
{
	_id = GUID::CreateGUID;

	for each (auto component in copy._components)
	{
		_components[typeid(component)] = CamelEngine::CreateComponent<typeid(component)>(_id);
	}
}

Entity::~Entity()
{
	for each (auto component in copy._components)
	{
		_components[typeid(component)] = CamelEngine::RemoveComponent<typeid(component)>(_id);
	}

	GUID::FreeGuid(_id);
}

template<class T> void Entity::AddComponent()
{
	_components[T] = CamelEngine::CreateComponent<T>(_id);
}

template<class T> void Entity::RemoveComponent()
{
	_components[T] = CamelEngine::RemoveComponent<T>(_id);
}

template<class T> const T& Entity::GetComponent()
{
	return CamelEngine::GetComponent<T>(_id);
}