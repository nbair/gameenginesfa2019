cmake_minimum_required(VERSION 3.12)

set(CMAKE_TOOLCHAIN_FILE "../vcpkg/scripts/buildsystems/vcpkg.cmake")

set(CMAKE_CXX_STANDARD 17)

project(CamelEngine)

file(GLOB_RECURSE fileCollection "../source/*.cpp" "../source/*.h" "../source/Components/*.cpp" "../source/Components/*.h" "../source/Core/*.cpp" "../source/Core/*.h" "../source/Systems/*.cpp" "../source/Systems/*.c" "../source/Systems/*.h" )

find_package(SDL2 CONFIG REQUIRED)
find_package(sdl2-mixer CONFIG REQUIRED)
find_package(assimp CONFIG REQUIRED)
find_package(glfw3 CONFIG REQUIRED)

add_executable(CamelEngine ${fileCollection})
add_library(CamelEngineLib STATIC ${fileCollection})

target_link_libraries(CamelEngine PRIVATE glfw)

target_link_libraries(CamelEngineLib PRIVATE glfw)

target_link_libraries(CamelEngine PRIVATE SDL2::SDL2 SDL2::SDL2main)
target_link_libraries(CamelEngine PRIVATE SDL2::SDL2_mixer)

target_link_libraries(CamelEngineLib PRIVATE SDL2::SDL2 SDL2::SDL2main)
target_link_libraries(CamelEngineLib PRIVATE SDL2::SDL2_mixer)