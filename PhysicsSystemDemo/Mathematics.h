#pragma once

#include <string>

class Vector2
{

};

class Vector3 {
public:
	Vector3(float x = 0.0f, float y = 0.0f, float z = 0.0f);

	Vector3& operator += (const Vector3 mth);
	Vector3& operator -= (const Vector3 mth);
	Vector3& operator *= (float mth);
	Vector3& operator /= (float mth);
	Vector3& operator = (const Vector3 mth);

	bool operator == (const Vector3& mth);
	bool operator != (const Vector3& mth);

	Vector3 operator+(const Vector3& mth) const;
	Vector3 operator-(const Vector3& mth) const;
	Vector3 operator*(float mth) const;
	Vector3 operator/(float mth) const;

	float x;
	float y;
	float z;

	const std::string ToString() const;
};

const Vector3 VECTOR3_ZERO(0.0f, 0.0f, 0.0f);

const Vector3 VECTOR3_ONE(1.0f, 1.0f, 1.0f);

void crossProduct(Vector3 one, Vector3 two, Vector3* product);

Vector3 lerp(Vector3 one, Vector3 two, float time);

class Matrix3
{

};

class Matrix4
{

};

class Quaternion
{
	public:
		inline const Vector3& EulerAngles() const
		{
			return VECTOR3_ZERO;
		}
};

const Quaternion QUATERNION_IDENTITY;