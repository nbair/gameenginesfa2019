#include "TransformComponent.h"

TransformComponent::TransformComponent() : _position(VECTOR3_ZERO), _orientation(QUATERNION_IDENTITY)
{}

TransformComponent::~TransformComponent()
{}

const Vector3& TransformComponent::GetPosition() const
{
	return _position;
}

const Quaternion& TransformComponent::GetOrientation() const
{
	return _orientation;
}

const Vector3& TransformComponent::EulerAngles() const
{
	return _orientation.EulerAngles();
}

void TransformComponent::SetPosition(const Vector3 position)
{
	_position = position;
}

void TransformComponent::Translate(const Vector3 translation)
{
	_position += translation;
}

void TransformComponent::Rotate(const Quaternion rotation)
{
	//_orientation += rotation;
}