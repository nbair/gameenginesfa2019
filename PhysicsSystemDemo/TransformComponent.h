#pragma once

#include "Component.h"
#include "Mathematics.h"

class TransformComponent : public Component
{
	public:

		TransformComponent();

		~TransformComponent();

		const Vector3& GetPosition() const;

		const Quaternion& GetOrientation() const;

		const Vector3& EulerAngles() const;

		void SetPosition(const Vector3 position);

		void Translate(const Vector3 translation);

		void Rotate(const Quaternion rotation);

	private:	

		Vector3 _position;

		Quaternion _orientation;
};