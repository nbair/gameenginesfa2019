#include "PhysicsComponent.h"
#include <iostream>
#include <string>

PhysicsComponent::PhysicsComponent() 
	: 
	_mass(0.0f), 
	_inverseMass(0.0f), 
	_targetTransform(nullptr)
{}

PhysicsComponent::~PhysicsComponent()
{}

void PhysicsComponent::SetMass(const float mass)
{
	_mass = mass >= 0.0f ? mass : -1.0f * mass;
	_inverseMass = _mass > 0.0f ? 1 / _mass : 0.0f;
}

void PhysicsComponent::SetTransformComponent(TransformComponent* transform)
{
	_targetTransform = transform;
}

const Vector3& PhysicsComponent::GetPosition() const
{
	return _targetTransform->GetPosition();
}

const Vector3& PhysicsComponent::GetVelocity() const
{
	return _velocity;
}

const Vector3& PhysicsComponent::GetAcceleration() const
{
	return _acceleration;
}

const Vector3& PhysicsComponent::GetAngularVelocity() const
{
	return _angularVelocity;
}

const Vector3& PhysicsComponent::GetAngularAcceleration() const
{
	return _angularAcceleration;
}

const float PhysicsComponent::GetMass() const
{
	return _mass;
}

const float PhysicsComponent::GetInverseMass() const
{
	return _inverseMass;
}

const Vector3& PhysicsComponent::GetMomentArm()
{
	//return _targetTransform->GetPosition() - _centerOfMassOffset;
	return VECTOR3_ZERO;
}

const float PhysicsComponent::GetMomentArmLength()
{
	/// TODO
	//return GetMomentArm().GetLength();
	return 1;
}

void PhysicsComponent::AddForce(const Vector3 force)
{
	_cumulativeForce += force;
}

void PhysicsComponent::AddTorque(const Vector3 torque)
{
	//_cumulativeTorque += torque;
}

void PhysicsComponent::Integrate(const float dt)
{
	_acceleration = _cumulativeForce * _inverseMass;
	Vector3 position = (_velocity * dt) + (_acceleration * 0.5 * dt * dt);
	_targetTransform->Translate(position);
	_velocity += _acceleration * dt;
	_cumulativeForce = VECTOR3_ZERO;
}