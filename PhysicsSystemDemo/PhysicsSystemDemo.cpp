#include <iostream>
#include "Physics.h"
#include "Entity.hpp"

int main()
{
	std::cout << "Creating new entity: ball" << std::endl;
	Entity* ball = new Entity("Ball");
	//Entity* ground = new Entity("Ground");

	std::cout << "Adding a transform at (0, 0) to the ball" << std::endl;
	TransformComponent transform = ball->AddComponent<TransformComponent>();
	transform.SetPosition(VECTOR3_ZERO);

	std::cout << "Setting the ball's mass to 1.0" << std::endl;
	PhysicsComponent physics = ball->AddComponent<PhysicsComponent>();
	physics.SetMass(1.0);
	physics.SetTransformComponent(&transform);

	std::cout << "Adding a collider to the ball" << std::endl;
	CollisionComponent collision = ball->AddComponent<CollisionComponent>();

	//ground->AddComponent<TransformComponent>();
	//ground->AddComponent<CollisionComponent>();

	float g = -9.8f;
	float t = 0.0f;

	system("pause");

	while (t < 60)
	{
		t += 1;

		system("CLS");

		std::cout << "Time: " << t << std::endl;
		std::cout << "Position: " << transform.GetPosition().ToString() << std::endl;
		std::cout << "Velocity: " << physics.GetVelocity().ToString() << std::endl;
		std::cout << "Acceleration: " << physics.GetAcceleration().ToString() << std::endl;

		physics.AddForce(Vector3(0, g, 0));
		physics.Integrate(0.1f);
	}

	std::cout << "Deleting the ball" << std::endl;
	delete ball;
}