#pragma once

#include <typeinfo>
#include <typeindex>
#include <unordered_map>
#include <list>

#include "Component.h"
#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "CollisionComponent.h"

typedef int EntityID;

class Entity
{
	public:

		inline Entity() {}

		inline Entity(const std::string& name) : _name(name) {}

		inline ~Entity()
		{
			for (auto pair : _components)
			{
				delete pair.second;
			}

			_components.clear();
		}

		template<class T> inline T& AddComponent()
		{
			_components[typeid(T)] = static_cast<T*>(new Component());
			return *static_cast<T*>(_components[typeid(T)]);
		}

		template<class T> inline void RemoveComponent()
		{
			delete _components[typeid(T)];
		}

		template<class T> inline T& GetComponent()
		{
			return *static_cast<T*>(_components[typeid(T)]);
		}

	private:

		std::string _name;

		std::unordered_map<std::type_index, Component*> _components;
};