#pragma once

#include "Component.h"
#include "TransformComponent.h"

class PhysicsComponent : public Component
{
	public:

		PhysicsComponent();

		~PhysicsComponent();


		void SetMass(const float mass);

		void SetTransformComponent(TransformComponent* transform);


		const Vector3& GetPosition() const;

		const Vector3& GetVelocity() const;

		const Vector3& GetAcceleration() const;

		const Vector3& GetAngularVelocity() const;

		const Vector3& GetAngularAcceleration() const;


		const float GetMass() const;

		const float GetInverseMass() const;

		const Vector3& GetMomentArm();

		const float GetMomentArmLength();


		void AddForce(const Vector3 force);

		void AddTorque(const Vector3 torque);

		void Integrate(const float dt);

	private:
	
		TransformComponent* _targetTransform;


		float _mass;

		float _inverseMass;

		Vector3 _centerOfMassOffset = VECTOR3_ZERO;


		Vector3 _velocity = VECTOR3_ZERO;

		Vector3 _acceleration = VECTOR3_ZERO;

		Vector3 _angularVelocity = VECTOR3_ZERO;

		Vector3 _angularAcceleration = VECTOR3_ZERO;


		Vector3 _cumulativeForce = VECTOR3_ZERO;
		
		Vector3 _cumulativeTorque = VECTOR3_ZERO;
};